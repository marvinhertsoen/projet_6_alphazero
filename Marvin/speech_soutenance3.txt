Les types de réseau de neurones diffèrent par plusieurs paramètres :

    la topologie des connexions entre les neurones ;
    la fonction d’agrégation utilisée (somme pondérée, distance pseudo-euclidienne…) ;
    la fonction de seuillage utilisée (sigmoïde, échelon, fonction linéaire, fonction de Gauss…) ;
    l’algorithme d’apprentissage (rétropropagation du gradient, cascade correlation) ;
    d’autres paramètres, spécifiques à certains types de réseaux de neurones, tels que la méthode de relaxation pour les réseaux de neurones (réseaux de Hopfield par exemple) qui ne sont pas à propagation simple (perceptron multicouche par exemple).


